import React from "react";

type ButtonProps = {
  text: string;
  type: "primary" | "secondary";
  onClick?: (e: any) => any;
};

export const Button = ({ text, type, onClick }: ButtonProps): JSX.Element => {
  const buttonClass =
    type === "secondary"
      ? "border border-current bg-white text-black"
      : "bg-cyan-500 text-white hover:bg-cyan-600";

  return (
    <button
      type="submit"
      className={`w-full h-12 px-6 rounded cursor-pointer text-base ${buttonClass}`}
      onClick={onClick}
    >
      {text}
    </button>
  );
};

export default Button;
