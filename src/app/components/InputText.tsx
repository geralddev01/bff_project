"use client";

import { ChangeEvent } from "react";

const controlStyle =
  "border border-gray-400 rounded block mb-1 px-4 py-2 w-full outline-none";

type InputTextProps = {
  name: string;
  value?: string;
  type: "text" | "textarea" | "password" | "email";
  label: string;
  placeholder: string;
  handleChange?: (
    event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => void;
  maxLength?: number;
};

export const InputText: React.FC<InputTextProps> = ({
  name,
  value,
  type,
  label,
  placeholder,
  handleChange,
  maxLength,
}) => {
  const renderControl = () => {
    if (type === "textarea") {
      return (
        <textarea
          name={name}
          value={value}
          className={controlStyle}
          placeholder={placeholder}
          onChange={handleChange}
          maxLength={maxLength}
        />
      );
    }

    return (
      <input
        aria-autocomplete="none"
        autoComplete="off"
        name={name}
        value={value}
        className={controlStyle}
        type={type}
        placeholder={placeholder}
        onChange={handleChange}
        maxLength={maxLength}
      />
    );
  };

  return (
    <span>
      <p className="font-semibold pb-2">{label}</p>
      {renderControl()}
    </span>
  );
};

export default InputText;
