"use client";
import { useState, useEffect } from "react";
import InputText from "../InputText";
import Popup from "../Popup";
import { showToast, validateForm, ValidationRulesType } from "@/utils/index";
import {
  createNote,
  editNote,
  getNoteById,
  NoteType,
} from "@/services/notes/index";

type CreateModalContentProps = {
  note_id?: number | null;
  openModal: boolean;
  toggleOpenModal: () => void;
  reloadData: () => void;
};

const createNoteValidationRules: ValidationRulesType = {
  name: [{ required: true }],
  description: [{ required: true }],
};
const initialState = {
  name: "",
  description: "",
};

const CreateNote: React.FC<CreateModalContentProps> = ({
  openModal,
  note_id,
  toggleOpenModal,
  reloadData,
}) => {
  useEffect(() => {
    const fetchData = async () => {
      if (note_id) {
        try {
          setErrors({});
          const response: any = await getNoteById(note_id);
          if (response) {
            setData({ name: response.name, description: response.description });
          }
        } catch (error) {
          console.error("Error fetching note:", error);
        }
      } else {
        setData(initialState);
      }
    };
    setErrors({});
    fetchData();
  }, [note_id]);

  const [data, setData] = useState<NoteType>(initialState);
  const [errors, setErrors] = useState<any>({});

  const handleChange = (
    event: React.ChangeEvent<
      HTMLInputElement | HTMLSelectElement | HTMLTextAreaElement
    >
  ) => {
    setData({
      ...data,
      [event.target.name]: event.target.value,
    });
    setErrors({
      ...errors,
      [event.target.name]: undefined,
    });
  };

  const handleRequestClick = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const formErrors = validateForm(data, createNoteValidationRules);

    if (Object.keys(formErrors).length > 0) {
      setErrors(formErrors);
      return;
    }
    if (!note_id) {
      const response = await createNote(data);
      if (response) {
        setData(initialState);
        reloadData();
        showToast("Note Created", "success");
      }
    } else {
      const response = await editNote({id: note_id, ...data});
      if (response) {
        setData(initialState);
        reloadData();
        showToast("Note Edited", "success");
      }
    }

    toggleOpenModal();
  };

  return (
    <Popup
      buttonLabel={`${note_id ? "Edit" : "Add"} Note`}
      isOpen={openModal}
      setIsOpen={toggleOpenModal}
      handleClick={handleRequestClick as any}
    >
      <h1 className="text-cyan-600 text font-bold text-xl mt-4 mb-4 mx-1">
        {note_id ? "Edit" : "Add"} Note
      </h1>
      <form onSubmit={handleRequestClick} className="form">
        <div className="mb-2">
          <InputText
            name="name"
            type="text"
            label="Note Title"
            placeholder="Note Title"
            value={data.name}
            handleChange={handleChange}
          />

          {errors.name && (
            <div className="text-dangerColor text-sm ml-1">{errors.name}</div>
          )}
        </div>

        <div className="mb-2">
          <InputText
            name="description"
            type="textarea"
            label="Note Description"
            placeholder="Description"
            value={data.description}
            handleChange={handleChange}
          />
          {errors.description && (
            <div className="text-dangerColor text-sm ml-1">
              {errors.description}
            </div>
          )}
        </div>
      </form>
    </Popup>
  );
};

export default CreateNote;
