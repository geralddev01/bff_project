import Popup from "@/components/Popup";
import { showToast } from "@/utils/index";
import Image from "next/image";
import trashIcon from "@/static/images/trash.svg";
import React, { FC } from "react";
import { deleteNote } from "@/services/notes/index";

type ConfirmAlertProps = {
  openAlert: boolean;
  toggleOpenAlert: () => void;
  reLoadNotes: () => void;
  id: number | null;
}

const ConfirmAlert: FC<ConfirmAlertProps> = ({
  openAlert,
  toggleOpenAlert,
  reLoadNotes,
  id,
}) => {
  
  const handleRequestClick = async () => {
    if (id) {
      const response = await deleteNote(id);
      if (response) {
        showToast("Note Deleted", "success");
      }
    }
    toggleOpenAlert();
    reLoadNotes();
  };

  return (
    <Popup
      buttonLabel="Yes delete"
      isOpen={openAlert}
      setIsOpen={toggleOpenAlert}
      handleClick={handleRequestClick}
    >
      <div className="flex justify-center flex-col items-center">
        <Image width={50} priority src={trashIcon} alt="trash Icon" />
        <h1 className="text-red-500 font-bold text-xl mt-4 mx-1">
          Delete this Note?
        </h1>
      </div>
    </Popup>
  );
};

export default ConfirmAlert;
