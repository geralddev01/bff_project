"use client";
import { getNotes } from "@/services/notes/index";
import FeatherIcon from "feather-icons-react";
import { useCallback, useEffect, useState } from "react";
import Button from "../Button";
import ConfirmAlert from "./ConfirmAlert";
import CreateNote from "./CreateNote";

type NotesType = {
  id: number;
  created_at: string;
  name: string;
  description: string;
  user_id: string;
};

export const NoteTableList: React.FC = () => {
  const [openModal, setOpenModal] = useState(false);
  const columns: string[] = ["id", "name", "description", "user_id"];
  const [openAlert, setOpenAlert] = useState<boolean>(false);
  const [data, setData] = useState<NotesType[]>([]);

  const [deleteNoteId, setDeleteNoteId] = useState<number | null>(null);
  const [editNoteId, setEditNoteId] = useState<number | null>(null);

  const loadNotes = useCallback(async () => {
    try {
      const notes: NotesType[] = await getNotes();
      if (notes) {
        setData(notes);
      }
    } catch (error) {
      console.error("Error loading Notes:", error);
    }
  }, []);

  useEffect(() => {
    loadNotes();
  }, [loadNotes]);

  const toggleOpenAlert = () => setOpenAlert((prevState) => !prevState);
  const toggleOpenModal = () => setOpenModal((prevState) => !prevState);

  const renderColumns = (): JSX.Element => (
    <>
      {columns.map((column) => (
        <th key={column}>{column}</th>
      ))}
      <th>Actions</th>
    </>
  );

  const renderRows = () => {
    if (data.length === 0) {
      return (
        <tr>
          <td colSpan={columns.length}>
            <span className="table-message flex items-center gap-5 justify-center">
              <FeatherIcon size="22" icon="alert-circle" /> No data
            </span>
          </td>
        </tr>
      );
    }
    return data.map((item, index) => (
      <tr key={index}>
        {columns.map((column) => (
          <td className="text-center text-cyan-800 font-medium" key={column}>
            {item[column as keyof NotesType]}
          </td>
        ))}
        <td className="text-center">
          <button
            className="mr-2"
            onClick={() => {
              toggleOpenAlert();
              setDeleteNoteId(item.id);
            }}
          >
            <FeatherIcon size="18" icon="trash-2" />
          </button>
          <button
            onClick={() => {
              setEditNoteId(item.id);
              toggleOpenModal();
            }}
          >
            <FeatherIcon size="18" icon="edit" />
          </button>
        </td>
      </tr>
    ));
  };

  return (
    <>
      <ConfirmAlert
        id={deleteNoteId}
        openAlert={openAlert}
        toggleOpenAlert={toggleOpenAlert}
        reLoadNotes={loadNotes}
      />
      <CreateNote
        note_id={editNoteId}
        openModal={openModal}
        toggleOpenModal={toggleOpenModal}
        reloadData={loadNotes}
      />

      <div className="w-2/4 mx-auto my-4">
        <Button
          onClick={() => {
            setEditNoteId(null);
            toggleOpenModal();
          }}
          type="primary"
          text="Create Note"
        />
      </div>

      <table className="w-full border-collapse">
        <thead>
          <tr>{renderColumns()}</tr>
        </thead>
        <tbody>{renderRows()}</tbody>
      </table>
    </>
  );
};
