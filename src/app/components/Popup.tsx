import React from "react";
import FeatherIcon from "feather-icons-react";
import Button from "./Button";

type PopupProps = {
  isOpen: boolean;
  setIsOpen: React.Dispatch<React.SetStateAction<boolean>>;
  children: React.ReactNode;
  buttonLabel: string;
  handleClick: () => void;
};

const Popup: React.FC<PopupProps> = ({
  isOpen,
  setIsOpen,
  children,
  buttonLabel,
  handleClick,
}) => {
  if (!isOpen) {
    return null;
  }

  const closePopup = () => {
    setIsOpen(false);
  };

  return (
    <div className="popup-container">
      <div className="popup-content">
        <div onClick={closePopup} className="popup-close cursor-pointer">
          <FeatherIcon strokeWidth="1.5" icon="x" />
        </div>
        <section>
          <div className="popup-inner">{children}</div>

          <div className="grid grid-cols-2 gap-2">
            <div className="btn btn-primary">
              <Button onClick={handleClick} text={buttonLabel} type="primary" />
            </div>
            <div className="btn">
              <Button onClick={closePopup} text="Cancel" type="secondary" />
            </div>
          </div>
        </section>
      </div>
    </div>
  );
};

export default Popup;
