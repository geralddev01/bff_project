import { useEffect, useState } from "react";
import { useRouter } from "next/navigation";
import { logOut } from "@/services/auth/index";

const Header = () => {
  const [user, setUser] = useState(null);
  const router = useRouter();
  useEffect(() => {
    const fetchUser = async () => {
      try {
        const userId = localStorage.getItem("userID");
        console.log(userId);

        if (userId) {
          console.log(userId);
        }
      } catch (error) {
        console.error("Error fetching user:", error);
      }
    };

    fetchUser();
  }, []);

  const closeSession = () => {
    logOut();
    router.push("/");
  };

  return (
    <nav className="bg-gray-800 p-4">
      <div className="max-w-7xl mx-auto flex justify-between items-center">
        <div className="flex items-center">
          <span className="text-white text-lg font-semibold">Hello, {}</span>
        </div>
        <div>
          <button
            className="bg-red-500 hover:bg-red-600 text-white font-bold py-2 px-4 rounded"
            onClick={() => closeSession()}
          >
            Logout
          </button>
        </div>
      </div>
    </nav>
  );
};

export default Header;
