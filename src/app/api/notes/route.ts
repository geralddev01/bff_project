import { createClient } from "@/utils/supabase/client";
import { NextResponse, NextRequest } from "next/server";

const supabase = createClient();

export const GET = async () => {
  let { data: Notes, error } = await supabase.from("Notes").select("*");

  if (error) {
    console.error("Error fetching notes:", error);
    return NextResponse.json(
      { message: "Failed to fetch notes", error: error.message },
      { status: 500 }
    );
  }

  return NextResponse.json(
    { message: "Notes fetched successfully", notes: Notes },
    { status: 200 }
  );
};

export const POST = async (request: NextRequest) => {
  try {
    const { name, description, user_id } = await request.json();
    const newNote = { name, description, user_id };

    const { data: noteData, error } = await supabase
      .from("Notes")
      .insert(newNote)
      .select();

    if (error) {
      console.error("Error creating note:", error);
      return NextResponse.json(
        { message: "Failed to create note", error: error.message },
        { status: 500 }
      );
    }
    if (!noteData) {
      return NextResponse.json(
        { message: "Failed to retrieve the note data after saving" },
        { status: 500 }
      );
    }

    return NextResponse.json(
      { message: "Note created successfully", note: newNote },
      { status: 201 }
    );
  } catch (error) {
    console.error("Error creating note:", error);
    return NextResponse.json(
      { message: "Failed to create note", error },
      { status: 500 }
    );
  }
};

export const DELETE = async (request: NextRequest) => {
  try {
    const { id } = await request.json();
    const { error } = await supabase.from("Notes").delete().eq("id", id);

    if (error) {
      console.error("Error deleting note:", error);
      return NextResponse.json(
        { message: "Failed to delete note", error: error.message },
        { status: 500 }
      );
    }

    return NextResponse.json(
      { message: "Note deleted successfully", id },
      { status: 200 }
    );
  } catch (error) {
    console.error("Error deleting note:", error);
    return NextResponse.json(
      { message: "Failed to delete note", error },
      { status: 500 }
    );
  }
};

export const PUT = async (request: NextRequest) => {
  try {
    const { id, name, description, user_id } = await request.json();
    const updatedNote = { name, description, user_id };

    const { data: updatedData, error } = await supabase
      .from("Notes")
      .update(updatedNote)
      .eq("id", id)
      .select();
    if (error) {
      console.error("Error updating note:", error);
      return NextResponse.json(
        { message: "Failed to update note", error: error.message },
        { status: 500 }
      );
    }

    if (!updatedData || updatedData.length === 0) {
      return NextResponse.json(
        { message: "Note not found for update" },
        { status: 404 }
      );
    }

    return NextResponse.json(
      { message: "Note updated successfully", note: updatedNote },
      { status: 200 }
    );
  } catch (error) {
    console.error("Error updating note:", error);
    return NextResponse.json(
      { message: "Failed to update note", error },
      { status: 500 }
    );
  }
};


