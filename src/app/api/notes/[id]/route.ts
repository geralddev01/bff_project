import { createClient } from "@/utils/supabase/client";
import { NextResponse, NextRequest } from "next/server";

const supabase = createClient();

export const GET = async (
  request: NextRequest,
  { params }: { params: { id: number } }
) => {
  try {
    const id = params.id;

    const { data: noteData, error } = await supabase
      .from("Notes")
      .select("*")
      .eq("id", id)
      .single();

    if (error) {
      console.error("Error fetching note:", error);
      return NextResponse.json(
        { message: "Failed to fetch note", error },
        { status: 500 }
      );
    }

    if (!noteData) {
      return NextResponse.json({ message: "Note not found" }, { status: 404 });
    }

    return NextResponse.json(
      { message: "Note fetched successfully", note: noteData },
      { status: 200 }
    );
  } catch (error) {
    console.error("Error fetching note:", error);
    return NextResponse.json(
      { message: "Failed to fetch note", error },
      { status: 500 }
    );
  }
};
