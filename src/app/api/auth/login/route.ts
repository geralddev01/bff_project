import { createClient } from "@/utils/supabase/client";
import config from "@/config";
import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken";
import { NextResponse, NextRequest } from "next/server";

const supabase = createClient();

export const POST = async (req: NextRequest, _res: NextResponse) => {
  try {
    const { email, password } = await req.json();

    const { data: users, error } = await supabase
      .from("Users")
      .select("*")
      .eq("email", email);

    if (error || !users[0]) {
      return NextResponse.json(
        { message: "ERROR invalid credentials", error },
        { status: 401 }
      );
    }

    const userFound = users[0];
    const matchPassword = await bcrypt.compare(password, userFound.password as string);

    if (!matchPassword) {
      return NextResponse.json(
        { message: "ERROR invalid credentials", error },
        { status: 401 }
      );
    }

    const token = jwt.sign({ id: userFound.id }, config.SECRET, {
      expiresIn: 86400, // 24 hrs
    });

    return NextResponse.json({ user: userFound, token }, { status: 200 });
  } catch (error) {
    console.error("Error logging in:", error);
    return NextResponse.json(
      { message: "ERROR invalid credentials", error },
      { status: 500 }
    );
  }
};
