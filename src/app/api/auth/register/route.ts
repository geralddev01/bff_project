import { createClient } from "@/utils/supabase/client";
import config from "@/config";
import jwt from "jsonwebtoken";
import { encryptPassword } from "@/utils/index";
import { NextResponse, NextRequest } from "next/server";

const supabase = createClient();

export const POST = async (req: NextRequest, _res: NextResponse) => {
  try {
    const { name, last_name, email, password } = await req.json();

    const encryptedPassword = await encryptPassword(password);

    const newUser = {
      name,
      last_name,
      email,
      password: encryptedPassword,
    };

    let { data: userData, error } = await supabase
      .from("Users")
      .insert([newUser])
      .select();

    if (error) {
      return NextResponse.json(
        { message: "Failed to register user", error: error.message },
        { status: 500 }
      );
    }

    if (!userData) {
      return NextResponse.json(
        { message: "Failed to retrieve user data after registration" },
        { status: 500 }
      );
    }

    const userId = userData[0].id;

    const token = jwt.sign({ id: userId }, config.SECRET, {
      expiresIn: 86400, // 24 hrs
    });

    return NextResponse.json({ user: userData, token }, { status: 201 });
  } catch (error) {
    console.error("Error registering user:", error);
    return NextResponse.json(
      { message: "Failed to register user", error },
      { status: 500 }
    );
  }
};
