import { createClient } from '@/utils/supabase/client';
import { NextResponse, NextRequest } from 'next/server';

const supabase = createClient()

export const GET = async () => {
  let { data: Users, error } = await supabase
  .from('Users')
  .select('*')

  if (error) {
    console.error('Error fetching users:', error);
    return NextResponse.json({ message: 'Failed to fetch users', error: error.message }, { status: 500 });
  }

  return NextResponse.json({ message: 'Users fetched successfully', users: Users }, { status: 200 });
};
