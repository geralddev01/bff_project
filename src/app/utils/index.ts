import bcrypt from "bcryptjs";
import { toast } from "react-toastify";
import { ToastOptions, ToastContent, ToastPosition } from 'react-toastify'; // Assuming toast options are from react-toastify library
// import {  } from "react-toastify/dist/types";

type ToastType = "info" | "success" | "warning" | "error";

type ShowToastOptions = {
  position?: ToastPosition;
  autoClose?: number;
  hideProgressBar?: boolean;
  closeOnClick?: boolean;
  pauseOnHover?: boolean;
};

export const showToast = (
  message: ToastContent,
  type: ToastType = "info",
  options: ShowToastOptions = {}
): void => {
  const defaultOptions: ToastOptions = {
    position: "top-right",
    autoClose: 4000,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: false,
  };

  const toastType = {
    info: toast.info,
    success: toast.success,
    warning: toast.warning,
    error: toast.error,
  }[type];

  if (!toastType) {
    console.error("Invalid toast type.");
    return;
  }

  const toastOptions: ToastOptions = { ...defaultOptions, ...options };

  toastType(message, toastOptions);
};

type FormDataType = {
  [key: string]: any;
};
export const hasItems = (array: Array<any>) =>
  Array.isArray(array) && array.length > 0;

type ValidationRule = {
  required?: boolean;
  pattern?: RegExp;
  message?: string;
};

export type ValidationRulesType = {
  [key: string]: ValidationRule[];
};

export const validateForm = <T extends FormDataType>(
  formData: T,
  validationRules: ValidationRulesType
): Partial<T> => {
  const errors: Partial<string | any> = {};

  for (const field in validationRules) {
    const rules = validationRules[field];

    for (const rule of rules) {
      if (rule.required && !formData[field]) {
        errors[field] = `${
          field.charAt(0).toUpperCase() + field.slice(1)
        } is required`;
        break;
      }

      if (rule.pattern && !rule.pattern.test(formData[field])) {
        errors[field] = rule.message || `Invalid ${field}`;
        break;
      }
    }
  }

  return errors;
};

export const encryptPassword = async (password: string) => {
  const salt = await bcrypt.genSalt(10);
  const passHash = await bcrypt.hash(password, salt);
  return passHash.toString();
};

export const loginValidationRules = {
  email: [
    { required: true },
    { pattern: /\S+@\S+\.\S+/, message: "Invalid email format" },
  ],
  password: [{ required: true }],
};


export const getFormattedDate = (date: Date) =>
  new Date(date).toLocaleDateString(undefined, {
    weekday: "long",
    month: "short",
    day: "numeric",
  });
