import { requestData } from "../axios/requestData";

export type UserLoginType = {
  email: string;
  password: string;
};

export const login = async (params: UserLoginType) => {
  const { email, password } = params;

  const data = {
    email,
    password,
  };

  let url = `auth/login`;

  try {
    const response = await requestData({
      method: "POST",
      url,
      data,
    });

    const { token, user } = response.data;

    localStorage.setItem("jwtToken", token);
    localStorage.setItem("userID", user[0].id);

    return response.data;
  } catch (error) {
    return { error };
  }
};

export type UserRegisterType = {
  name: string;
  last_name: string;
  email: string;
  password: string;
};

export const register = async (params: UserRegisterType) => {
  const { name, last_name, email, password } = params;

  const data = {
    name,
    last_name,
    email,
    password,
  };

  let url = `auth/register`;

  try {
    const response = await requestData({
      method: "POST",
      url,
      data,
    });

    const { token, user } = response.data;

    localStorage.setItem("jwtToken", token);
    localStorage.setItem("userID", user[0].id);

    return response.data;
  } catch (error) {
    return { error };
  }
};

export const logOut = () => {
  try {
    localStorage.removeItem("jwtToken");
    localStorage.removeItem("userID");
    return { message: "Logged out successfully" };
  } catch (error) {
    return { error };
  }
};

export const getUser = async (id: string) => {
  let url = `users/${id}`;

  try {
    const response = await requestData({
      method: "GET",
      url,
    });

    return response.data;
  } catch (error) {
    return { error };
  }
};
