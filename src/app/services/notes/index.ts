import { requestData } from "../axios/requestData";

export type NoteType = {
  id?: number;
  name: string;
  description: string;
};

export const getNotes = async () => {
  let url = `/notes`;

  try {
    const response = await requestData({
      method: "GET",
      url,
    });
    return response.data.notes;
  } catch (error) {
    return { error };
  }
};

export const deleteNote = async (id: number) => {
  let url = `/notes`;

  try {
    const response = await requestData({
      method: "DELETE",
      url,
      data: { id },
    });
    return response;
  } catch (error) {
    return { error };
  }
};

export const createNote = async (
  params: NoteType = { name: "", description: "" }
) => {
  const userId = localStorage.getItem("userID");
  const { name, description } = params;

  const data = {
    user_id: userId,
    name,
    description,
  };

  let url = `/notes`;

  try {
    const response = await requestData({
      method: "POST",
      url,
      data,
    });

    return response.data;
  } catch (error) {
    return { error };
  }
};

export const editNote = async (
  params: NoteType = { name: "", description: "" }
) => {
  const userId = localStorage.getItem("userID");
  const { id, name, description } = params;

  const data = {
    id,
    user_id: userId,
    name,
    description,
  };

  let url = `/notes`;

  try {
    const response = await requestData({
      method: "PUT",
      url,
      data,
    });

    return response.data;
  } catch (error) {
    return { error };
  }
};

export const getNoteById = async (id: number) => {
  let url = `/notes/${id}`;

  try {
    const response = await requestData({
      method: "GET",
      url,
    });
    return response.data.note;
  } catch (error) {
    return { error };
  }
};
