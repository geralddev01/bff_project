"use client";
import "@/static/styles/global.css";
import "react-toastify/dist/ReactToastify.css";
import React, { useState, ReactElement, FormEvent, ChangeEvent } from "react";
import { InputText } from "@/components/InputText";
import Link from "next/link";
import { Button } from "@/components/Button";
import { UserLoginType, login } from "@/services/auth/index";
import loginImage from "@/static/images/login.svg";
import { useRouter } from "next/navigation";
import Image from "next/image";
import {
  hasItems,
  showToast,
  validateForm,
  ValidationRulesType,
} from "@/utils/index";
import { ToastContainer } from "react-toastify";

export const Login = (): ReactElement => {
  const router = useRouter();
  const initialState = {
    email: "",
    password: "",
  };
  const [userData, setUserData] = useState<UserLoginType>(initialState);
  const [errors, setErrors] = useState<{ [key: string]: string | undefined }>(
    {}
  );

  const loginValidationRules: ValidationRulesType = {
    email: [
      { required: true },
      { pattern: /\S+@\S+\.\S+/, message: "Invalid email format" },
    ],
    password: [{ required: true }],
  };

  const handleLoginSubmit = async (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const formErrors = validateForm(userData, loginValidationRules);

    if (hasItems(Object.keys(formErrors))) {
      setErrors(formErrors);
      return;
    }

    const response = await login(userData);

    if (!response.token) {
      showToast("Login failed. Please check your credentials", "error");
      setUserData(initialState);
      return;
    }

    showToast(`Welcome ${response.username}`, "success");
    router.push("/notes");
  };

  const handleChange = (event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>): void => {
    const { name, value } = event.target;
    setUserData({
      ...userData,
      [name]: value,
    });
    setErrors({
      ...errors,
      [name]: undefined,
    });
  };

  return (
    <div className="mx-auto my-6 max-w-2xl ">
      <div className=" w-full flex justify-center">
        <Image src={loginImage} width={200} alt="login user" />
      </div>
      <ToastContainer className="z-full-zIndex" />
      <form
        onSubmit={handleLoginSubmit}
        className="my-6 shadow-lg rounded-md p-10"
      >
        <h1 className="font-bold text-center mt-4 text-2xl text-primary ">
          LOGIN
        </h1>
        <p className="text-center text-lg font-bold my-2 text-successColor">
          Sign into your account
        </p>

        <div className="mb-2">
          <InputText
            name="email"
            type="text"
            label="Email address"
            placeholder="Email address"
            value={userData.email}
            handleChange={handleChange}
          />

          {errors.email && (
            <div className="text-dangerColor text-sm ml-1">{errors.email}</div>
          )}
        </div>
        <div className="mb-2">
          <InputText
            name="password"
            type="password"
            label="Password"
            placeholder="Password"
            value={userData.password}
            handleChange={handleChange}
          />
          {errors.password && (
            <div className="text-dangerColor text-sm ml-1">
              {errors.password}
            </div>
          )}
        </div>

        <div className="flex w-full px-4 py-2">
          <Button text="Sign In" type="primary" />
        </div>
      </form>
      <p className="my-1 text-center">
        You dont have an account?
        <Link className="text-primary" href="/register">
          Register
        </Link>
      </p>
    </div>
  );
};

export default Login;
