import Link from "next/link";
import { ReactElement } from "react";
import "@/static/styles/global.css";
import "@/static/styles/index.css";
import Button from "@/components/Button";

const Home = (): ReactElement => {
  return (
    <div>
      <section className="landing">
        <div className="dark-overlay">
          <div className="landing-inner">
            <h1 className="font-extrabold text-4xl mb-8">
              COOLEST NOTES APP
            </h1>
            <p className="text-lg mb-4 font-bold">
              YOU CAN CREATE, LIST, AND MANAGE ALL YOUR IMPORTANT NOTES
            </p>
            <div className="grid grid-cols-2 gap-2">
              <Link href="/register" className="btn btn-primary">
                <Button text="Sign Up" type="primary" />
              </Link>
              <Link href="/login" className="btn">
              <Button text="Login" type="secondary" />
              </Link>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default Home;
