import "@/static/styles/global.css";
import "@/static/styles/index.css";
import "react-toastify/dist/ReactToastify.css";
import { ReactElement } from "react";
import notesImage from "@/static/images/notes.svg";
import Image from "next/image";
import { NoteTableList } from "@/components/notes/NoteTableList";
import Header from "@/components/Header";

const Notes = (): ReactElement => {
  return (
    <>
      <Header />
      <div className="mx-auto my-6  w-full ">
        <div className="flex justify-center">
          <Image src={notesImage} width={200} alt="notes user" />
        </div>
        <div className="p-5">
          <NoteTableList />
        </div>
      </div>
    </>
  );
};
export default Notes;
