"use client";
import "@/static/styles/global.css";
import "react-toastify/dist/ReactToastify.css";
import React, { useState, FormEvent, ReactElement } from "react";
import { InputText } from "@/components/InputText";
import { Button } from "@/components/Button";
import loginImage from "@/static/images/login.svg";
import Image from "next/image";
import Link from "next/link";
import { UserRegisterType, register } from "@/services/auth/index";
import { useRouter } from "next/navigation";
import { ToastContainer } from "react-toastify";
import {
  hasItems,
  showToast,
  validateForm,
  ValidationRulesType,
} from "@/utils/index";

export const Register = (): ReactElement => {
  const router = useRouter();
  const initialState = {
    name: "",
    last_name: "",
    email: "",
    password: "",
  };
  const [userData, setUserData] = useState<UserRegisterType>(initialState);
  const [errors, setErrors] = useState<{ [key: string]: string | undefined }>(
    {}
  );

  const RegisterValidationRules: ValidationRulesType = {
    name: [{ required: true }],
    last_name: [{ required: true }],
    email: [
      { required: true },
      { pattern: /\S+@\S+\.\S+/, message: "Invalid email format" },
    ],
    password: [{ required: true }],
  };

  const handleRegisterSubmit = async (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const formErrors = validateForm(userData, RegisterValidationRules);

    if (hasItems(Object.keys(formErrors))) {
      setErrors(formErrors);
      return;
    }

    const response = await register(userData);

    if (!response.token) {
      showToast("Login failed. Please check your credentials", "error");
      setUserData(initialState);
      return;
    }


    showToast(`Welcome ${response.name}`, "success");
    router.push("/notes");
  };

  const handleChange = (event: any): void => {
    const { name, value } = event.target;
    setUserData({
      ...userData,
      [name]: value,
    });
    setErrors({
      ...errors,
      [name]: undefined,
    });
  };

  return (
    <div className="mx-auto my-6 max-w-2xl ">
      <div className=" w-full flex justify-center">
        <Image src={loginImage} width={200} alt="login user" />
      </div>
      <ToastContainer className="z-full-zIndex" />
      <form
        onSubmit={handleRegisterSubmit}
        className="my-6 shadow-lg rounded-md p-10"
      >
        <h1 className="font-bold text-center mt-4 text-2xl text-primary ">
          REGISTER
        </h1>

        <div className="mb-2">
          <InputText
            name="name"
            type="text"
            label="Name"
            value={userData.name}
            placeholder="Your Name"
            handleChange={handleChange}
          />
          {errors.name && (
            <div className="text-dangerColor text-sm ml-1">{errors.name}</div>
          )}
        </div>

        <div className="mb-2">
          <InputText
            name="last_name"
            type="text"
            label="Last Name"
            value={userData.last_name}
            placeholder="Your Last Name"
            handleChange={handleChange}
          />
          {errors.last_name && (
            <div className="text-dangerColor text-sm ml-1">
              {errors.last_name}
            </div>
          )}
        </div>
        <div className="mb-2">
          <InputText
            name="email"
            type="text"
            label="Email address"
            placeholder="Email address"
            value={userData.email}
            handleChange={handleChange}
          />
          {errors.email && (
            <div className="text-dangerColor text-sm ml-1">{errors.email}</div>
          )}
        </div>
        <div className="mb-2">
          <InputText
            name="password"
            type="password"
            label="Create Password"
            placeholder="Password"
            value={userData.password}
            handleChange={handleChange}
          />
          {errors.password && (
            <div className="text-dangerColor text-sm ml-1">
              {errors.password}
            </div>
          )}
        </div>

        {/* <div className="mb-2">
          <InputText
            name="password"
            type="password"
            label="Repeat Password"
            placeholder="Password"
            value={userData.password}
            handleChange={handleChange}
          />
          {errors.password && <InputError error={errors.password} />}
        </div> */}
        {/* <input type="submit" value="Register" className="btn btn-primary" /> */}
        <div className="flex w-full px-4 py-2">
          <Button text="Register" type="primary" />
        </div>
      </form>

      <p className="my-1 text-center">
        Already have an account?{" "}
        <Link className="text-primary" href="/login">
          Login
        </Link>
      </p>
    </div>
  );
};

export default Register;
